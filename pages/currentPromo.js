const { I } = inject();

module.exports = {
  // setting locators
  fields: {
    searchResults: "#primary h1",
  },
  links: {
    currentTitle: "h1.entry-title",
    previousPromo: ".nav-previous a",
  },

  // introducing methods
  clickOnPreviousPromo() {
    I.click(this.links.previousPromo);
  },

  getCurrentTitleText() {
    const currentTitleText = I.grabTextFrom(this.links.currentTitle);
    return currentTitleText;
  },

  getPreviousPromoTitle() {
    const previousPromoTitle = I.grabTextFrom(this.links.previousPromo);
    return previousPromoTitle;
  },

  getSearchResultsTitle() {
    const searchResultsTitle = I.grabTextFrom(this.fields.searchResults);
    return searchResultsTitle;
  },
};

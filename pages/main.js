const { I } = inject();

module.exports = {
// setting locators
links: {
  promoPhoto: '#primary div.post.type-post:first-child .post-thumb a',
  promoTitle: '#primary div.post.type-post:first-child .entry-title a',
  promoMoreLink: '#primary div.post.type-post:first-child .more-link'
},

// introducing methods
clickOnPromoPhoto() {
  I.click(this.links.promoPhoto);
},

clickOnMoreLink() {
  I.click(this.links.promoMoreLink);
},

getPromoTitle() {
const promoTitleText = I.grabTextFrom(this.links.promoTitle)
return promoTitleText;
},
}

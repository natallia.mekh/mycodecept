const { I } = inject();

module.exports = {
  fields: {
    searchInput: "#searchform input",
  },
  links: {
    logo: "#logo",
  },
  clickOnLogo() {
    I.click(this.links.logo);
  },

  searchPromo(searchText) {
    I.click(this.fields.searchInput);
    I.fillField(this.fields.searchInput, searchText);
    I.pressKey(["Enter"]);
  },

  searchPromoText(searchedText) {
    const searhResultText = "Search Results for “" + searchedText + "”";
    return searhResultText.toUpperCase();
  },

};

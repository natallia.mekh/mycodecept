const assert = require("assert");

Feature("Тесты с использованием codecept");

Scenario(
  "Пользователь может просмотреть акцию, кликнув по картинке с акцией",
  async ({ I, mainPage, currentPromoPage }) => {
    I.amOnPage("https://vandrouki.by/");
    const promoTitleText = await mainPage.getPromoTitle();
    mainPage.clickOnPromoPhoto();
    const currentTitleText = await currentPromoPage.getCurrentTitleText();
    assert.equal(currentTitleText, promoTitleText);
  }
);

Scenario(
  "Пользователь может просмотреть акцию, кликнув по тексту Читать далее",
  async ({ I, mainPage, currentPromoPage }) => {
    I.amOnPage("https://vandrouki.by/");
    const promoTitleText = await mainPage.getPromoTitle();
    mainPage.clickOnMoreLink();
    const currentTitleText = await currentPromoPage.getCurrentTitleText();
    assert.equal(currentTitleText, promoTitleText);
  }
);

Scenario(
  "Пользователь может просмотреть предыдущую акцию, кликнув по ссылке <-",
  async ({ I, mainPage, currentPromoPage }) => {
    I.amOnPage("https://vandrouki.by/");
    mainPage.clickOnMoreLink();

    const previousPromoTitleText = await currentPromoPage.getPreviousPromoTitle();
    currentPromoPage.clickOnPreviousPromo();

    const currentTitleText = await currentPromoPage.getCurrentTitleText();
    assert.equal(currentTitleText, previousPromoTitleText);
  }
);

Scenario(
  "Пользователь может вернуться на главную страницу, кликнув по лого",
  ({ I, mainPage, headerMenuPage }) => {
    I.amOnPage("https://vandrouki.by/");
    mainPage.clickOnPromoPhoto();
    headerMenuPage.clickOnLogo();
    I.seeInCurrentUrl("https://vandrouki.by/");
  }
);

Scenario(
  "Пользователь может найти акцию, используя поиск",
  async ({ I, currentPromoPage, headerMenuPage }) => {
    I.amOnPage("https://vandrouki.by/");
    headerMenuPage.searchPromo("минск");
    const transformedSearchResultText = headerMenuPage.searchPromoText("минск");
    const searchResultsTitle = await currentPromoPage.getSearchResultsTitle();
    assert.equal(searchResultsTitle, transformedSearchResultText);
  }
);
